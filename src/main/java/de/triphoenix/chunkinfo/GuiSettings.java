package de.triphoenix.chunkinfo;

import com.mumfrey.liteloader.client.gui.GuiCheckbox;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class GuiSettings extends GuiScreen {

	private GuiCheckbox toggleshowBiome, toggleshowFPS, toggleshowCoords,
			toggleshowChunk, toggleshowCoordsInChunk, toggleshowCompass,
			toggleshowPlacement, toggleshowCompassColor, toggleshowEntities,
			toggleshowLightlevel, toggleshowArmour, toggleshowUsage,
			toggleshowEntityCounts, togglelargeWarning, toggleshowRedstonePower,
			togglesortPlayerList;

	private Slider itemDamageSlider;
	
	private ChunkInfo chunkinfo;

	public GuiSettings() {
		chunkinfo = LiteModChunkInfo.chunkinfo;
	}

	@Override
	public void initGui() {

		int posX = width / 2 - 155;
		int posY = height / 8;

		toggleshowChunk = new GuiCheckbox(1, posX + 160 * ((4 - 1) % 2), posY + 20 * ((4 - 1) / 2), getButtonText(1));
		toggleshowCoords = new GuiCheckbox(2, posX + 160 * ((3 - 1) % 2), posY + 20 * ((3 - 1) / 2), getButtonText(2));
		toggleshowCoordsInChunk = new GuiCheckbox(3, posX + 160 * ((5 - 1) % 2), posY + 20 * ((5 - 1) / 2), getButtonText(3));
		toggleshowPlacement = new GuiCheckbox(4, posX + 160 * ((7 - 1) % 2), posY + 20 * ((7 - 1) / 2), getButtonText(4));
		toggleshowCompass = new GuiCheckbox(5, posX + 160 * ((6 - 1) % 2), posY + 20 * ((6 - 1) / 2), getButtonText(5));
		toggleshowCompassColor = new GuiCheckbox(6, posX + 160 * ((8 - 1) % 2), posY + 20 * ((8 - 1) / 2), getButtonText(6));
		toggleshowBiome = new GuiCheckbox(7, posX + 160 * ((1 - 1) % 2), posY + 20 * ((1 - 1) / 2), getButtonText(7));
		toggleshowFPS = new GuiCheckbox(8, posX + 160 * ((2 - 1) % 2), posY + 20 * ((2 - 1) / 2), getButtonText(8));
		toggleshowEntities = new GuiCheckbox(9, posX + 160 * ((9 - 1) % 2), posY + 20 * ((9 - 1) / 2), getButtonText(9));
		toggleshowLightlevel = new GuiCheckbox(10, posX + 160 * ((10 - 1) % 2), posY + 20 * ((10 - 1) / 2), getButtonText(10));
		toggleshowUsage = new GuiCheckbox(11, posX + 160 * ((12 - 1) % 2), posY + 20 * ((12 - 1) / 2), getButtonText(11));
		toggleshowArmour = new GuiCheckbox(12, posX + 160 * ((11 - 1) % 2), posY + 20 * ((11 - 1) / 2), getButtonText(12));
		toggleshowEntityCounts = new GuiCheckbox(13, posX + 160 * ((13 - 1) % 2), posY + 20 * ((13 - 1) / 2), getButtonText(13));
		togglelargeWarning = new GuiCheckbox(14, posX + 160 * ((14 - 1) % 2), posY + 20 * ((14 - 1) / 2), getButtonText(14));
		toggleshowRedstonePower = new GuiCheckbox(15, posX + 160 * ((15 - 1) % 2), posY + 20 * ((15 - 1) / 2), getButtonText(15));
		
		// Spielerliste
		togglesortPlayerList = new GuiCheckbox(16, posX + 160 * ((16 - 1) % 2), posY + 20 * ((16 - 1) / 2), getButtonText(16));

		toggleshowBiome.checked = chunkinfo.showBiome;
		toggleshowFPS.checked = chunkinfo.showFPS;
		toggleshowCoords.checked = chunkinfo.showCoords;
		toggleshowChunk.checked = chunkinfo.showChunk;
		toggleshowCoordsInChunk.checked = chunkinfo.showCoordsInChunk;
		toggleshowCompass.checked = chunkinfo.showCompass;
		toggleshowPlacement.checked = chunkinfo.showPlacement;
		toggleshowCompassColor.checked = chunkinfo.showCompassColor;
		toggleshowEntities.checked = chunkinfo.showEntities;
		toggleshowLightlevel.checked = chunkinfo.showLightlevel;
		toggleshowArmour.checked = chunkinfo.showArmour;
		toggleshowUsage.checked = chunkinfo.showUsage;
		toggleshowEntityCounts.checked = chunkinfo.showEntityCounts;
		togglelargeWarning.checked = chunkinfo.largeWarning;
		toggleshowRedstonePower.checked = chunkinfo.showRedstonePower;
		
		togglesortPlayerList.checked = chunkinfo.sortPlayerList;


		buttonList.add(toggleshowBiome);
		buttonList.add(toggleshowFPS);
		buttonList.add(toggleshowCoords);
		buttonList.add(toggleshowChunk);
		buttonList.add(toggleshowCoordsInChunk);
		buttonList.add(toggleshowCompass);
		buttonList.add(toggleshowPlacement);
		buttonList.add(toggleshowCompassColor);
		buttonList.add(toggleshowEntities);
		buttonList.add(toggleshowLightlevel);
		buttonList.add(toggleshowArmour);
		buttonList.add(toggleshowUsage);
		buttonList.add(toggleshowEntityCounts);
		buttonList.add(togglelargeWarning);
		buttonList.add(toggleshowRedstonePower);
		
		buttonList.add(togglesortPlayerList);

		
		/*
		 * Slider
		 */

		itemDamageSlider = new Slider(20, posX + 160 * ((17 - 1) % 2), posY + 20 * ((17 - 1) / 2), 15, 50, chunkinfo.itemDamageWarning, getButtonText(20));
		buttonList.add(itemDamageSlider);
	}
	
	@Override
	protected void actionPerformed(GuiButton guibutton) {

		boolean checked = false;
		if (guibutton instanceof GuiCheckbox) {
			((GuiCheckbox) guibutton).checked = !((GuiCheckbox) guibutton).checked;	
			checked = ((GuiCheckbox) guibutton).checked;
		}
		switch(guibutton.id){
			case 1: chunkinfo.showChunk = !chunkinfo.showChunk; break;
			case 2: chunkinfo.showCoords = !chunkinfo.showCoords; break;
			case 3: chunkinfo.showCoordsInChunk = !chunkinfo.showCoordsInChunk; break;
			case 4: chunkinfo.showPlacement = !chunkinfo.showPlacement; break;
			case 5: chunkinfo.showCompass = !chunkinfo.showCompass; break;
			case 6: chunkinfo.showCompassColor = !chunkinfo.showCompassColor; break;
			case 7: chunkinfo.showBiome = !chunkinfo.showBiome; break;
			case 8: chunkinfo.showFPS = !chunkinfo.showFPS; break;
			case 9: chunkinfo.showEntities = !chunkinfo.showEntities; break;
			case 10: chunkinfo.showLightlevel = !chunkinfo.showLightlevel; break;
			case 11: chunkinfo.showUsage = !chunkinfo.showUsage; break;
			case 12: chunkinfo.showArmour = !chunkinfo.showArmour; break;
			case 13: chunkinfo.showEntityCounts = !chunkinfo.showEntityCounts; break;
			case 14: chunkinfo.largeWarning = !chunkinfo.largeWarning; break;
			case 15: chunkinfo.showRedstonePower = !chunkinfo.showRedstonePower; break;
			
			case 16: chunkinfo.sortPlayerList = checked; break;

		}

		chunkinfo.writeConfig();
	}

	@Override
	public void drawScreen(int par1, int par2, float par3) {
		drawDefaultBackground();
		
		drawCenteredString(fontRenderer, "ChunkInfo settings", width / 2, 20, 16777215);
		
		if(itemDamageSlider.value != chunkinfo.itemDamageWarning){
			chunkinfo.itemDamageWarning = itemDamageSlider.value;
			chunkinfo.writeConfig();
		}
		
		super.drawScreen(par1, par2, par3);
	}
	
	private String getButtonText(int id) {
		switch (id) {

			case 1:
				return "Show Chunk";
			case 2:
				return "Show Coordinates";
			case 3:
				return "Chunk-local Coords";
			case 4:
				return "Block Placement";
			case 5:
				return "Compass";
			case 6:
				return "Colored Compass";
			case 7:
				return "Show Biome";
			case 8:
				return "Show FPS";
			case 9:
				return "Show Entity Count";
			case 10:
				return "Show Light Level";
			case 11:
				return "Show Item Usage";
			case 12:
				return "Show Armour Warning";
			case 13:
				return "Show animal count";
			case 14:
				return "Large tool warning";
			case 15:
				return "Show Redstone Power";
			
			// TAB
			case 16: 
				return "Sort PlayerList";
			
			// Slider
			case 20: 
				return "Damage Warning: ";	
			}
		return "";
	}
}
