package de.triphoenix.chunkinfo;

import com.google.common.base.Optional;
import com.google.gson.annotations.Expose;
import com.mumfrey.liteloader.core.LiteLoader;
import com.mumfrey.liteloader.modconfig.ConfigStrategy;
import com.mumfrey.liteloader.modconfig.Exposable;
import com.mumfrey.liteloader.modconfig.ExposableOptions;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDeadBush;
import net.minecraft.block.BlockRedstoneRepeater;
import net.minecraft.block.BlockRedstoneTorch;
import net.minecraft.block.BlockRedstoneWire;
import net.minecraft.block.BlockSnowBlock;
import net.minecraft.block.BlockTallGrass;
import net.minecraft.block.BlockVine;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityGolem;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ClassInheritanceMultiMap;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.Chunk;

@ExposableOptions(strategy = ConfigStrategy.Versioned, filename="chunkinfo.json")
public class ChunkInfo implements Exposable {
	
	Minecraft mc = Minecraft.getMinecraft();

	public final int COLOR_WHITE = 16777215;
	private final String[] compassStrings = { "Z+", "...", "...", "...", "X-",
			"...", "...", "...", "Z-", "...", "...", "...", "X+", "...", "...",
			"...", "Z+" };
	
	@Expose
	public boolean showBiome = true;
	@Expose
	public boolean showFPS = true;
	@Expose
	public boolean showCoords = true;
	@Expose
	public boolean showChunk = true;
	@Expose
	public boolean showCoordsInChunk = true;
	@Expose
	public boolean showCompass = true;
	@Expose
	public boolean showPlacement = true;
	@Expose
	public boolean showCompassColor = true;
	@Expose
	public boolean showEntities = true;
	@Expose
	public boolean showLightlevel = true;
	@Expose
	public boolean showArmour = true;
	@Expose
	public boolean showUsage = true;
	@Expose
	public boolean showEntityCounts = true;
	@Expose
	public boolean largeWarning = true;
	@Expose
	public boolean showRedstonePower = true;
	@Expose
	public int itemDamageWarning = 15;
	@Expose
	public boolean sortPlayerList = false;

	
	
	private int colorForAngle(int angle) {
		if (!showCompassColor) {
			return 16777215;
		}
		if (angle <= 90) {
			return 16711680 + ((int) (angle / 90.0D * 255.0D) << 8);
		}
		if (angle <= 180) {
			return 65280 + (255 - (int) ((angle - 90) / 90.0D * 255.0D) << 16);
		}
		if (angle <= 270) {
			int val = (int) ((angle - 180) / 90.0D * 255.0D);
			return (val / 2 << 8) + (255 - val << 8) + val;
		}
		int val = (int) ((angle - 270) / 90.0D * 255.0D);
		return 255 - val + (val << 16) + ((255 - val) / 2 << 8);
	}
	
	private int[] countAnimals(Minecraft mc, int chunkx, int chunkz) {
		Chunk chunk = mc.world.getChunkFromBlockCoords(new BlockPos(chunkx, 32, chunkz));
		int[] result = new int[3];
		for (ClassInheritanceMultiMap<Entity> entityList : chunk.getEntityLists()) {
			for (Entity o : entityList) {
			
				if(o.getDisplayName().getFormattedText().startsWith("\u00A72") || o.getDisplayName().getFormattedText().startsWith("\u00A7c")){				
					if ((o instanceof EntityAnimal)) {						
						result[0] += 1;
					} else if ((o instanceof EntityVillager)) {
						result[1] += 1;
					}
				}
				if ((o instanceof EntityGolem)) {
					result[2] += 1;
				}
			}
		}
		return result;
	}

	public void displayHook(Minecraft mc) {
		if (mc.gameSettings.showDebugInfo || (!mc.inGameHasFocus)) {
			return;
		}

		int currentY = 5;

		int chX = mc.player.chunkCoordX;
		int chZ = mc.player.chunkCoordZ;
		int posX = (int) mc.player.posX;
		int posY = (int) mc.player.posY;
		int posZ = (int) mc.player.posZ;
		BlockPos playerBlockPos = new BlockPos(posX, Math.max(0, Math.min(255, posY)), posZ);
		
		if (mc.player.posX < 0.0D) {
			
			posX--;
		}
		if (mc.player.posZ < 0.0D) {
			
			posZ--;
		}
		FontRenderer fontRenderer = mc.fontRenderer;
		currentY = displayChunkHook(currentY, chX, chZ, fontRenderer);
		currentY = displayCoordsHook(currentY, chX, chZ, posX, posY, posZ, fontRenderer);
		currentY = displayCompassHook(mc, currentY, fontRenderer);
		currentY = displayPlacementHook(mc, currentY, fontRenderer);
		currentY = displayBiomeHook(mc, currentY, posX, posZ, playerBlockPos, fontRenderer);
		currentY = displayFPSHook(mc, currentY, fontRenderer);
		currentY = displayEntityCountsHook(mc, currentY, posX, posZ, fontRenderer);
		currentY = displayLightlevelHook(mc, currentY, playerBlockPos, fontRenderer);
		currentY = displayEntitiesHook(mc, currentY, fontRenderer);
		currentY = displayRedstonePowerHook(mc, currentY, fontRenderer);

		int alertColor = calculateAlertColor(mc);
		ScaledResolution scaledRes = new ScaledResolution(mc);
		int yPos = displayUsageHook(mc, fontRenderer, alertColor, scaledRes);
		displayArmourHook(mc, fontRenderer, alertColor, yPos);
	}

	private int displayEntitiesHook(Minecraft mc, int currentY, FontRenderer fontRenderer) {
		if (showEntities) {
			String eDebug = mc.renderGlobal.getDebugInfoEntities();
			int indexDot = eDebug.indexOf(',');

			String entitySubstring = indexDot >= 3 ? eDebug.substring(3, indexDot) : eDebug;
			renderString(fontRenderer, "Entities: " + entitySubstring, 5.0F, currentY, 16777215);
			currentY += 10;
		}
		return currentY;
	}

	private void displayArmourHook(Minecraft mc, FontRenderer fontRenderer, int alertColor, int yPos) {
		
		if (showArmour) {
			int needsMessage = 0;
			String[] messages = new String[4];
					
			for (int i = 0; i < mc.player.inventory.armorInventory.size(); i++) {

				ItemStack is = mc.player.inventory.armorInventory.get(i);

				if (is != ItemStack.EMPTY) {
					
					if(is.isItemStackDamageable()){
						
						float armorDamage = (float) is.getItemDamage() / is.getMaxDamage();
						
						if (armorDamage >= 0.85) {						
							messages[needsMessage] = is.getDisplayName();
							needsMessage++;
						}
					}
				}
			}

			if (needsMessage > 0) {
				renderString(fontRenderer, "Armor warning!", 5.0F, yPos - 20 - 10 * needsMessage, alertColor);
				for (int i = 0; i < needsMessage; i++) {
					renderString(fontRenderer, messages[i], 5.0F, yPos - 10 - 10 * needsMessage + 10 * i, alertColor);
				}
			}
		}
	}

	private int displayUsageHook(Minecraft mc, FontRenderer fontRenderer, int alertColor, ScaledResolution scaledRes) {
		int yPos = scaledRes.getScaledHeight();
		if (showUsage) {
			ItemStack currentItem = mc.player.inventory.getCurrentItem();
			if ((currentItem != null) && (currentItem.getMaxDamage() >= 29)) {
				int usesLeft = currentItem.getMaxDamage() - currentItem.getItemDamage();

				StringBuilder outputString = new StringBuilder();
				outputString.append("Item uses left: ").append(usesLeft);

				int color = 16777215;
				if (usesLeft < 15) {
					color = alertColor;
				}
				if ((usesLeft < itemDamageWarning) && (largeWarning)) {
					String output = outputString.toString();
					int textWidth = fontRenderer.getStringWidth(output);

					int x = (int) ((scaledRes.getScaledWidth() - textWidth * 2.0F) / 2.0F);
					int y = scaledRes.getScaledHeight() * 2 / 3;

					GlStateManager.pushMatrix();
					GlStateManager.scale(2.0F, 2.0F, 1.0F);
					renderString(fontRenderer, outputString.toString(), (int) (x / 2.0F), (int) (y / 2.0F), color);
					GlStateManager.popMatrix();
				} else {
					renderString(fontRenderer, outputString.toString(), 5.0F, yPos - 10, color);
				}
			}
		}
		return yPos;
	}

	private int displayBiomeHook(Minecraft mc, int currentY, int posX,
			int posZ, BlockPos playerBlockPos, FontRenderer fontRenderer) {
		if (showBiome) {
			Chunk chunk = mc.world.getChunkFromBlockCoords(new BlockPos( posX, 32, posZ));
			Biome biomeGen = chunk.getBiome(playerBlockPos, mc.world.getBiomeProvider());
			renderString(fontRenderer, "Biome: " + biomeGen.getBiomeName(), 5.0F, currentY, 16777215);
			currentY += 10;
		}
		return currentY;
	}

	private int displayCoordsHook(int currentY, int chX, int chZ, int posX, int posY, int posZ, FontRenderer fontRenderer) {
		
		if (showCoords) {
			if (showCoordsInChunk) {
				renderString(fontRenderer, "X: " + posX + " (in chunk " + (posX - chX * 16) + ")", 5.0F, currentY, 16777215);
				renderString(fontRenderer, "Y: " + posY, 5.0F, currentY + 10, 16777215);
				renderString(fontRenderer, "Z: " + posZ + " (in chunk " + (posZ - chZ * 16) + ")", 5.0F, currentY + 20, 16777215);

				currentY += 30;
			} else {
				renderString(fontRenderer, "X: " + posX, 5.0F, currentY, 16777215);
				renderString(fontRenderer, "Y: " + posY, 5.0F, currentY + 10, 16777215);
				renderString(fontRenderer, "Z: " + posZ, 5.0F, currentY + 20, 16777215);
				currentY += 30;
			}
		} else if (showCoordsInChunk) {
			renderString(fontRenderer, "X in chunk: " + (posX - chX * 16), 5.0F, currentY, 16777215);
			renderString(fontRenderer, "Z in chunk: " + (posZ - chZ * 16), 5.0F, currentY + 10, 16777215);
			currentY += 20;
		}
		return currentY;
	}

	private int displayChunkHook(int currentY, int chX, int chZ, FontRenderer fontRenderer) {
		if (showChunk) {
			renderString(fontRenderer, "Current chunk: " + chX + ", " + chZ, 5.0F, currentY, 16777215);
			currentY += 10;
		}
		return currentY;
	}

	private int displayEntityCountsHook(Minecraft mc, int currentY, int posX, int posZ, FontRenderer fontRenderer) {
		if (showEntityCounts) {
			int[] animalCounts = countAnimals(mc, posX, posZ);
			renderString(fontRenderer, "In Chunk (Animal/Vill/Gol): " + animalCounts[0] + "/" + animalCounts[1] + "/" + animalCounts[2], 5.0F, currentY, 16777215);
			currentY += 10;
		}
		return currentY;
	}

	private int displayLightlevelHook(Minecraft mc, int currentY, BlockPos playerBlockPos, FontRenderer fontRenderer) {
		if (showLightlevel) {
			
			Chunk curChunk = mc.world .getChunkFromBlockCoords(playerBlockPos);			
			int blockLight = curChunk.getLightFor(EnumSkyBlock.BLOCK, playerBlockPos);
			int skyLight = curChunk.getLightFor(EnumSkyBlock.SKY, playerBlockPos);

			renderString(fontRenderer, "Light: " + skyLight + "/" + blockLight, 5.0F, currentY, 16777215);
			currentY += 10;
		}
		return currentY;
	}

	private int calculateAlertColor(Minecraft mc) {
		long time = mc.world.getWorldInfo().getWorldTime();
		int alertTime = (int) (time % 32L);
		if (alertTime >= 16) {
			alertTime = 32 - alertTime;
		}
		if (alertTime == 16) {
			alertTime = 15;
		}
		int alertColor = 16711680 + (alertTime << 4 << 8);
		return alertColor;
	}

	private int displayFPSHook(Minecraft mc, int currentY, FontRenderer fontRenderer) {
		if (showFPS) {
			int bracesPos = mc.debug.indexOf('(');
			String fpsInfo = bracesPos == 0 ? mc.debug : mc.debug.substring(0, bracesPos);
			renderString(fontRenderer, fpsInfo, 5.0F, currentY, 16777215);
			currentY += 10;
		}
		return currentY;
	}

	private int displayRedstonePowerHook(Minecraft mc, int currentY, FontRenderer fontRenderer) {
		if (showRedstonePower) {
			RayTraceResult mouseObjectPos = mc.objectMouseOver;

			Optional<Integer> blockPower;
			if (mouseObjectPos != null) {
				BlockPos objectPos = mouseObjectPos.getBlockPos();

				if (objectPos != null) {
					IBlockState blockState = mc.world .getBlockState(objectPos);
					Block block = blockState.getBlock();

					if ((block instanceof BlockRedstoneWire)) {
						blockPower = Optional.of((Integer) blockState.getValue(BlockRedstoneWire.POWER));
					} else {

						if (((block instanceof BlockRedstoneRepeater)) || ((block instanceof BlockRedstoneTorch))) {
							blockPower = Optional.of(Integer.valueOf(16));
						} else {
							blockPower = Optional.absent();
						}
					}
				} else {
					blockPower = Optional.absent();
				}
			} else {
				blockPower = Optional.absent();
			}
			if (blockPower.isPresent()) {
				int xPos = 5;
				int powerValue = blockPower.get().intValue();

				xPos = renderString(fontRenderer, "RS Power: [", xPos, currentY, 16777215);
				for (int i = 0; i < 16; i++) {
					if (i < powerValue) {
						int renderColor = renderColorForRedstonePower(i);
						renderString(fontRenderer, "|", xPos, currentY, renderColor);
					}
					xPos += 2;
				}
				renderString(fontRenderer, "] " + blockPower.get(), xPos, currentY, 16777215);
			} else {
				renderString(fontRenderer, "RS Power: ---", 5.0F, currentY, 16777215);
			}
			currentY += 10;
		}
		return currentY;
	}

	private int renderColorForRedstonePower(int i) {

		int renderColor;
		if (i < 3) {
			renderColor = 16729156;
		} else {

			if (i < 8) {
				renderColor = 16777028;
			} else {
				renderColor = 4521796;
			}
		}
		return renderColor;
	}

	private int displayPlacementHook(Minecraft mc, int currentY, FontRenderer fontRenderer) {
		if (showPlacement) {
			RayTraceResult mouseObjectPos = mc.objectMouseOver;
			if (mouseObjectPos != null) {
				BlockPos objectPos = mouseObjectPos.getBlockPos();
				if (objectPos != null) {
					int objX = objectPos.getX();
					int objY = objectPos.getY();
					int objZ = objectPos.getZ();

					Block block = mc.world.getBlockState(objectPos) .getBlock();
					EnumFacing objectSide = mc.objectMouseOver.sideHit;
					if ((!(block instanceof BlockSnowBlock)) && (!(block instanceof BlockVine)) && (!(block instanceof BlockTallGrass)) && (!(block instanceof BlockDeadBush)) && objectSide != null) {
						switch (objectSide) {
						case DOWN:
							objY--;
							break;
						case UP:
							objY++;
							break;
						case NORTH:
							objZ--;
							break;
						case SOUTH:
							objZ++;
							break;
						case WEST:
							objX--;
							break;
						case EAST:
							objX++;
						}
					}
					EnumFacing orientation = EnumFacing.getDirectionFromEntityLiving(new BlockPos( objX, objY, objZ), mc.player);
					String oStr = "???";
					switch (orientation) {
					case UP:
						oStr = "up (Y+)";
						break;
					case DOWN:
						oStr = "down (Y-)";
						break;
					case NORTH:
						oStr = "Z+";
						break;
					case SOUTH:
						oStr = "Z-";
						break;
					case WEST:
						oStr = "X+";
						break;
					case EAST:
						oStr = "X-";
					}
					renderString(fontRenderer, "Place: " + oStr, 5.0F, currentY, 16777215);
				}
			} else {
				renderString(fontRenderer, "Place: ---", 5.0F, currentY, 16777215);
			}
			currentY += 10;
		}
		return currentY;
	}

	private int displayCompassHook(Minecraft mc, int currentY, FontRenderer fontRenderer) {
		if (showCompass) {
			int angle = (int) mc.player.rotationYaw % 360;
			if (angle < 0) {
				angle += 360;
			}
			int dir = (int) ((angle + 11.25D) / 22.5D);

			renderString(fontRenderer, "Dir:", 5.0F, currentY, 16777215);

			int currentX = 30;
			for (int i = -3; i <= 3; i++) {
				int curDir = (16 + dir + i) % 16;
				int curAngle = (int) (curDir * 22.5D);
				renderString(fontRenderer, compassStrings[curDir], currentX, currentY, colorForAngle(curAngle));
				currentX += 15;
			}
			currentY += 10;
		}
		return currentY;
	}

	public int renderString(FontRenderer fontRenderer, String p_175063_1_, float p_175063_2_, float p_175063_3_, int p_175063_4_) {
		return fontRenderer.drawStringWithShadow(p_175063_1_, p_175063_2_, p_175063_3_, p_175063_4_);
	}

	public void writeConfig() {
		LiteLoader.getInstance().writeConfig(this);
	}
}
