package de.triphoenix.chunkinfo;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.math.MathHelper;

public class Slider extends GuiButton {

		private float sliderValue;
		public int value;
		public boolean dragging;
		private final float minimum;
		private final float maximum;
		private String label;

		public Slider(int buttonID, int xPos, int yPos, float minimum, float maximum, int currentValue, String label) 
		{
			super(buttonID, xPos, yPos, 130, 20, "");

			this.minimum = minimum;
			this.maximum = maximum;
			this.sliderValue = normalize(currentValue);
			this.value = currentValue;
			this.label = label;
			this.displayString = label + value;
		}

		/**
		 * Returns 0 if the button is disabled, 1 if the mouse is NOT hovering over
		 * this button and 2 if it IS hovering over this button.
		 */
		@Override
		protected int getHoverState(boolean mouseOver) {
			return 0;
		}

		/**
		 * Fired when the mouse button is dragged. Equivalent of
		 * MouseListener.mouseDragged(MouseEvent e).
		 */

		@Override
		protected void mouseDragged(Minecraft mc, int mouseX, int mouseY) {

			if (this.visible) {
				if (this.dragging) {
					this.sliderValue = (float) (mouseX - (this.x + 4)) / (float) (this.width - 8);
					this.sliderValue = MathHelper.clamp(this.sliderValue, 0.0F, 1.0F);

					this.value = (int) denormalize(this.sliderValue);
					this.displayString = label + value;
				}

				mc.getTextureManager().bindTexture(BUTTON_TEXTURES);
				GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
				this.drawTexturedModalRect(this.x + (int) (this.sliderValue * this.width - 8), this.y, 0, 66, 4, 20);
				this.drawTexturedModalRect(this.x + (int) (this.sliderValue * this.width - 8) + 4, this.y, 196, 66, 4, 20);
			}
		}

		/**
		 * Returns true if the mouse has been pressed on this control. Equivalent of
		 * MouseListener.mousePressed(MouseEvent e).
		 */
		@Override
		public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {

			if (super.mousePressed(mc, mouseX, mouseY)) {
				this.sliderValue = (float) (mouseX - (this.x + 4)) / (float) (this.width - 8);
				this.sliderValue = MathHelper.clamp(this.sliderValue, 0.0F, 1.0F);

				this.value = (int) denormalize(this.sliderValue);
				this.displayString = label + value;

				this.dragging = true;
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Fired when the mouse button is released. Equivalent of
		 * MouseListener.mouseReleased(MouseEvent e).
		 */
		@Override
		public void mouseReleased(int mouseX, int mouseY) {
			this.dragging = false;
		}

		private float denormalize(float value) {
			return Math.round(minimum + (maximum - minimum) * value);

		}

		private float normalize(float value) {
			return (value - minimum) / (maximum - minimum);
		}
	
}
