package de.triphoenix.chunkinfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.annotation.Nullable;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;
import com.mojang.authlib.GameProfile;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiIngame;
import net.minecraft.client.gui.GuiPlayerTabOverlay;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EnumPlayerModelParts;
import net.minecraft.scoreboard.IScoreCriteria;
import net.minecraft.scoreboard.ScoreObjective;
import net.minecraft.scoreboard.ScorePlayerTeam;
import net.minecraft.scoreboard.Scoreboard;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.GameType;

public class GuiPlayerTab extends GuiPlayerTabOverlay {

	private Minecraft mc;
	private ChunkInfo chunkInfo;
	
	public GuiPlayerTab(Minecraft mcIn, GuiIngame guiIngameIn, ChunkInfo chunkInfo) {
		super(mcIn, guiIngameIn);
		mc = mcIn;
		this.chunkInfo = chunkInfo;
	}

	public void renderPlayerlist() {
		
    	ScaledResolution scaledresolution = new ScaledResolution(mc);
        int width = scaledresolution.getScaledWidth();
    	
        Scoreboard scoreboardIn = mc.world.getScoreboard();
        
        @Nullable
        ScoreObjective scoreObjectiveIn = scoreboardIn.getObjectiveInDisplaySlot(0);
        
        NetHandlerPlayClient nethandlerplayclient = mc.player.connection;
        Ordering<NetworkPlayerInfo> ENTRY_ORDERING = Ordering.from(new GuiPlayerTab.PlayerComparator());

        List<NetworkPlayerInfo> rawList = ENTRY_ORDERING.<NetworkPlayerInfo>sortedCopy(nethandlerplayclient.getPlayerInfoMap());
		
        List<NetworkPlayerInfo> list = new ArrayList<NetworkPlayerInfo>();
        
        
        int rows = 20;
    	list = rawList.subList(0, Math.min(rawList.size(), 80));
    	
        if(chunkInfo.sortPlayerList){
        	if(LiteModChunkInfo.isKadcon){
        		rows = 30;
        		
    			list = rawList.subList(Math.min(rawList.size(), 80), rawList.size());
    			
        		// R�nge setzen
        		List<NetworkPlayerInfo> teamList = rawList.subList(22, 35);
        		
        		for(int i = 0; i < 13; i++){
        			
        			if(teamList.get(i) != null){
        				if(teamList.get(i).getDisplayName() != null){
        					if(teamList.get(i).getDisplayName().getUnformattedText().equals("")){
        						break;
        					}else{
        						// Namen analysieren
        						int first =  teamList.get(i).getDisplayName().getUnformattedText().indexOf("]");
        						String teamMemberName = teamList.get(i).getDisplayName().getUnformattedText().substring(first + 3);
        						
        						// Namen durch Rang erweitern
        						for(NetworkPlayerInfo player : list){
        							if(player.getGameProfile().getName().equals(teamMemberName)){
        								player.setDisplayName(teamList.get(i).getDisplayName());
        							}
        						}					
        					}	
        				}
        			}
        		}
        		Collections.sort(list, new GuiPlayerInfoSorter());
        	}
        }
        	
		int i = 0;
		int j = 0;

		for (NetworkPlayerInfo networkplayerinfo : list) {
			int k = mc.fontRenderer.getStringWidth(this.getPlayerName(networkplayerinfo));
			i = Math.max(i, k);

			if (scoreObjectiveIn != null && scoreObjectiveIn.getRenderType() != IScoreCriteria.EnumRenderType.HEARTS) {
				k = mc.fontRenderer.getStringWidth(" " + scoreboardIn.getOrCreateScore(networkplayerinfo.getGameProfile().getName(), scoreObjectiveIn).getScorePoints());
				j = Math.max(j, k);
			}
		}

		int l3 = list.size();
		int i4 = l3;
		int j4;

		
		for (j4 = 1; i4 > rows; i4 = (l3 + j4 - 1) / j4) {
			++j4;
		}

		boolean flag = mc.isIntegratedServerRunning() || mc.getConnection().getNetworkManager().isEncrypted();
		int l;

		if (scoreObjectiveIn != null) {
			if (scoreObjectiveIn.getRenderType() == IScoreCriteria.EnumRenderType.HEARTS) {
				l = 90;
			} else {
				l = j;
			}
		} else {
			l = 0;
		}

		int i1 = Math.min(j4 * ((flag ? 9 : 0) + i + l + 13), width - 50) / j4;
		int j1 = width / 2 - (i1 * j4 + (j4 - 1) * 5) / 2;
		int k1 = 10;
		int l1 = i1 * j4 + (j4 - 1) * 5;

		
		drawRect(width / 2 - l1 / 2 - 1, k1 - 1, width / 2 + l1 / 2 + 1, k1 + i4 * 9, Integer.MIN_VALUE);

		for (int k4 = 0; k4 < l3; ++k4) {
			int l4 = k4 / i4;
			int i5 = k4 % i4;
			int j2 = j1 + l4 * i1 + l4 * 5;
			int k2 = k1 + i5 * 9;
			drawRect(j2, k2, j2 + i1, k2 + 8, 553648127);
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			GlStateManager.enableAlpha();
			GlStateManager.enableBlend();
			GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);

			if (k4 < list.size()) {
				NetworkPlayerInfo networkplayerinfo1 = (NetworkPlayerInfo) list.get(k4);
				GameProfile gameprofile = networkplayerinfo1.getGameProfile();

				if (flag) {
					EntityPlayer entityplayer = mc.world.getPlayerEntityByUUID(gameprofile.getId());
					boolean flag1 = entityplayer != null && entityplayer.isWearing(EnumPlayerModelParts.CAPE) && ("Dinnerbone".equals(gameprofile.getName()) || "Grumm".equals(gameprofile.getName()));
					mc.getTextureManager().bindTexture(networkplayerinfo1.getLocationSkin());
					int l2 = 8 + (flag1 ? 8 : 0);
					int i3 = 8 * (flag1 ? -1 : 1);
					Gui.drawScaledCustomSizeModalRect(j2, k2, 8.0F, (float) l2, 8, i3, 8, 8, 64.0F, 64.0F);

					if (entityplayer != null && entityplayer.isWearing(EnumPlayerModelParts.HAT)) {
						int j3 = 8 + (flag1 ? 8 : 0);
						int k3 = 8 * (flag1 ? -1 : 1);
						Gui.drawScaledCustomSizeModalRect(j2, k2, 40.0F, (float) j3, 8, k3, 8, 8, 64.0F, 64.0F);
					}

					j2 += 9;
				}

				String s4 = this.getPlayerName(networkplayerinfo1);

				if (networkplayerinfo1.getGameType() == GameType.SPECTATOR) {
					mc.fontRenderer.drawStringWithShadow(TextFormatting.ITALIC + s4, (float) j2, (float) k2, -1862270977);
				} else {
					mc.fontRenderer.drawStringWithShadow(s4, (float) j2, (float) k2, -1);
				}

				if(!chunkInfo.sortPlayerList){
					this.drawPing(i1, j2 - (flag ? 9 : 0), k2, networkplayerinfo1);
				}
				
			}
		}

	}

	static class PlayerComparator implements Comparator<NetworkPlayerInfo> {

		private PlayerComparator() {
		}

		public int compare(NetworkPlayerInfo p_compare_1_, NetworkPlayerInfo p_compare_2_) {
			ScorePlayerTeam scoreplayerteam = p_compare_1_.getPlayerTeam();
			ScorePlayerTeam scoreplayerteam1 = p_compare_2_.getPlayerTeam();
			return ComparisonChain.start()
					.compareTrueFirst(p_compare_1_.getGameType() != GameType.SPECTATOR,
							p_compare_2_.getGameType() != GameType.SPECTATOR)
					.compare(scoreplayerteam != null ? scoreplayerteam.getName() : "",
							scoreplayerteam1 != null ? scoreplayerteam1.getName() : "")
					.compare(p_compare_1_.getGameProfile().getName(), p_compare_2_.getGameProfile().getName()).result();
		}

	}
	
	   static class GuiPlayerInfoSorter implements Comparator<NetworkPlayerInfo> {

	    	private int getPlayerRank(String playerName) {
	    		
	    		if (playerName.startsWith("\u00A77") || playerName.startsWith("\u00A7bV\u00A78")) {		// VIP und normale
	    			return 0;
	    		}
	    		if (playerName.startsWith("\u00A78[\u00A7a")) {		// Supporter
	    			return 1;
	    		}
	    		if (playerName.startsWith("\u00A78[\u00A79")) { 	// Moderator/SMod
	    			return 2;
	    		}
	    		if (playerName.startsWith("\u00A78[\u00A76")) { 	// Smod+
	    			return 3;
	    		}
	    		if (playerName.startsWith("\u00A70[\u00A74")) {		// Admin und die Putze
	    			return 4;
	    		}
	    		return 0;
	    	}

	    	@Override
	    	public int compare(NetworkPlayerInfo arg1, NetworkPlayerInfo arg2) {

	    		String name1 = arg1.getDisplayName() != null ? arg1.getDisplayName().getFormattedText() : ScorePlayerTeam.formatPlayerName(arg1.getPlayerTeam(), arg1.getGameProfile().getName());
	    		String name2 = arg2.getDisplayName() != null ? arg2.getDisplayName().getFormattedText() : ScorePlayerTeam.formatPlayerName(arg2.getPlayerTeam(), arg2.getGameProfile().getName());

	    		// VIP: 		�bV�8| �fDrBlackWolf�r
	    		// NICHT VIP: 	�7HueyLuise�r
	    		
	    		// Paragraph: 	\u00A7
	    		
	    		// Supporter: 	�8[�aSup�8] �fLexyDev�r
	    		// Moderator: 	�8[�9M+�8] �fd4n1el89�r
		
	    		int rank1 = getPlayerRank(name1);
	    		int rank2 = getPlayerRank(name2);

	    		int diff = rank2 - rank1;
	    		if (diff == 0) {
	        		String sName1 = name1.replace("\u00A77", "").replace("\u00A7bV\u00A78| \u00A7f", ""); // �7 und �bV�8| �f entfernen
	        		String sName2 = name2.replace("\u00A77", "").replace("\u00A7bV\u00A78| \u00A7f", ""); // �7 und �bV�8| �f entfernen
	    			return sName1.compareToIgnoreCase(sName2);
	    		}
	    		return diff;
	    	}
	    }
}

//}
