package de.triphoenix.chunkinfo;

import java.io.File;
import org.lwjgl.input.Keyboard;

import com.mojang.realmsclient.dto.RealmsServer;
import com.mumfrey.liteloader.JoinGameListener;
import com.mumfrey.liteloader.Tickable;
import com.mumfrey.liteloader.core.LiteLoader;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.network.INetHandler;
import net.minecraft.network.play.server.SPacketJoinGame;

public class LiteModChunkInfo implements Tickable, JoinGameListener {

	public final String NAME = "ChunkInfo";
	public final String VERSION = "1.9";
	
	public static ChunkInfo chunkinfo = new ChunkInfo();

	private KeyBinding keySettings, keyPlayerlist;
	
	public static boolean isKadcon = false;

	GuiPlayerTab playerList = new GuiPlayerTab(Minecraft.getMinecraft(), Minecraft.getMinecraft().ingameGUI, chunkinfo);

	@Override
	public void onTick(Minecraft mc, float partialTicks, boolean inGame, boolean clock) {

		if (keySettings.isPressed() && mc.inGameHasFocus) {
			mc.displayGuiScreen(new GuiSettings());
		}
		
		if (keyPlayerlist.isKeyDown() && mc.inGameHasFocus) {
			playerList.renderPlayerlist();
		}else{
			chunkinfo.displayHook(mc);
		}
		
	}	
	
	@Override
	public void init(File configPath) {

		keySettings = new KeyBinding("Settings", Keyboard.KEY_F4, "ChunkInfo");
		keyPlayerlist = new KeyBinding("List Players", Keyboard.KEY_TAB, "ChunkInfo");

		LiteLoader.getInput().registerKeyBinding(keySettings);
		LiteLoader.getInput().registerKeyBinding(keyPlayerlist);

		LiteLoader.getInstance().registerExposable(chunkinfo, null);
		chunkinfo.writeConfig();
	}
	
	@Override
	public String getVersion() {
		return VERSION;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void upgradeSettings(String version, File configPath,File oldConfigPath) { }

	@Override
	public void onJoinGame(INetHandler netHandler, SPacketJoinGame joinGamePacket, ServerData serverData, RealmsServer realmsServer) {
		
		isKadcon = false;
		
		if(serverData != null){
			if(serverData.serverIP.toLowerCase().contains("kadcon")){
				isKadcon = true;
			}
		}	
	}
}